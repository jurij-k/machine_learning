#!/usr/bin/env python

import numpy as np

######
# Main
######

def main():
	# Definition of matrix
	M = np.array([[1,2], [3,4], [5,6]])
	print M

	# Mean of each column.
	mean = np.mean(M, axis=0)

	# Subtract column means
	C = M - mean

	# Covariance matrix.
	V = np.cov(C.T)

	# Eigenvalues and vectors.
	values, vectors = np.linalg.eig(V)

	# Project data.
	P = vectors.T.dot(C.T)

	print P.T

if __name__ == '__main__':
	main()