#!/usr/bin/env python
"""
Example for linear regression based on self implemented and scikit-learn.
"""

import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression

class Gradient(object):
    """ Gradient descent. """

    def __init__(self, theta, alpha=0.0001):
        self.alpha = alpha
        self.theta = theta

    def derive(self, X, y):
        n = float(X.shape[0])
        m = sum(np.sum(X * self.theta, axis=1) - y) / n
        x = sum(X[:,1] * (np.sum(X * self.theta, axis=1) - y)) / n
        return m, x

    def step_gradient(self, X, y):
        m, x = self.derive(X, y)
        self.theta[0] = self.theta[0] - self.alpha * m
        self.theta[1:] = self.theta[1:] - self.alpha * x
        print("step gradient - m: %0.2f m_new: %0.2f x: %0.2f x_new: %0.2f" %(m, self.theta[0], x, self.theta[1]))

    def fit(self, X, y, iter):
        for i in xrange(iter):
            self.step_gradient(X,y)

    def predict(self, X):
        return np.sum(X * self.theta, axis=1)

    def error(self, X, y):
        n = float(X.shape[0])
        squared_errors = sum((y - np.sum(X * self.theta, axis=1)) ** 2)
        return squared_errors / n


#def train_regr_gradient(X, y):
#    return RegressionGradient().fit(X,y)

def train_regr_sklearn(X, y):
    return LinearRegression().fit(X, y)

def main():
    # Create example data
    X = np.array([[1,2], [2,4], [3,5], [4,6], [7,10]])
    y = [3,6,8,10,17]

    print("Using sklearn.")
    regr_sklearn = train_regr_sklearn(X, y)
    print regr_sklearn.predict(X)

    print("Using gradient descent.")
    grad = Gradient()
    grad.fit(X,y)

    # Plot if data consists of two dimensions.
    if X.shape[1] == 1:
        plt.scatter(X, y, color='black')
        plt.plot(X, regr_sklearn.predict(X), color='blue')
        plt.xticks()
        plt.yticks()
        plt.show()

if __name__ == "__main__":
    main()
