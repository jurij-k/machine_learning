import pytest
import numpy as np

from regression import linear_regression as lr


def test_error():
    """ Testing squarred error function. """
    X = np.array([[1,2], [2,3]])
    y = [3,5]
    grad = lr.Gradient(theta=[0,1])
    assert grad.error(X,y) == 2.5