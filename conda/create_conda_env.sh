#!/usr/bin/env bash
# Creation of conda evironment

cd $(dirname ${0})

NAME=${1}
ENV_FILE=${2}

conda env create --name ${1} -f ${2}

