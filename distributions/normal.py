#!/usr/bin/env python
"""
Standard distribution.
"""
import argparse
import math as m
import matplotlib.pyplot as plt

def density(x, mu, sigma):
    """  """
    return (1. / (2 * m.pi * sigma ** 2)**0.5) * m.exp(- ((float(x) - mu) ** 2) / (2 * sigma ** 2) )

def distribution(x_range, mu, sigma):
    values = []
    for x in x_range:
        values.append(density(x, mu, sigma))
    return values

def main():
    """ Calculation and visualization of std distribution. """

    parser = argparse.ArgumentParser(description='Normal distribution.')
    parser.add_argument('--mu', type=float, nargs='+', help='Mu values.')
    parser.add_argument('--sigma', type=float, nargs='+', help='Sigma values.')
    parser.add_argument('--x-lower-bound', type=int, default=-5, help='Lower value for x axis.')
    parser.add_argument('--x-upper-bound', type=int, default=5, help='Upper value for x axis.')
    args = parser.parse_args()

    print args.mu
    print args.sigma


    if len(args.mu) != len(args.sigma):
        raise argparse.ArgumentTypeError("Number of mu and sigma values must be the same.")

    x_range = [x / 10. for x in xrange(args.x_lower_bound * 10, args.x_upper_bound * 10)]

    for i in xrange(len(args.mu)):
        plt.plot(x_range, distribution(x_range, args.mu[i], args.sigma[i]))

    plt.show()

if __name__ == "__main__":
    main()