#!/usr/bin/env python

import numpy as np
from math import e

def softmax(vec):
	denominator = sum(map(lambda x: e**x, vec))
	dist = map(lambda x: e**x / denominator, vec)
	return dist

def main():
	examples = []
	examples.append([1,2,3,4,5,6])
	examples.append([0.1, 0.2, 0.3, 0.4])
	examples.append([1,2,3,2,1])
	examples.append([1,2,4,8,4,2,1])

	for elem in examples:
		print elem
		print (softmax(elem))

if __name__ == '__main__':
	main()