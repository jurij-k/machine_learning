#!/usr/bin/env python

import numpy as np
import math as m
from collections import defaultdict


def subsampling(word_pairs, vocab):
    n = len(word_pairs)
    result = {}
    for word in set(word_pairs):
        fraction = vocab[word] / float(n)
        result[word] = (m.sqrt(fraction / 0.001) + 1) * 0.001 / fraction

    return result

def build_vocab(word_pairs):
    result = defaultdict(int)
    for word_pair in word_pairs:
        for word in word_pair:
            result[word] += 1

    return result

def main():
    word_pairs = [('a', 'b'), ('a', 'd'), ('a', 'f'), ('a', 'h')]
    vocab = build_vocab(word_pairs)
    print vocab
    print subsampling(word_pairs, vocab)


if __name__ == '__main__':
    main()