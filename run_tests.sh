#!/usr/bin/env bash

cd "$(dirname "$0")"

export PYTHONPATH="./src:${PYTHONPATH}"

pytest --cache-clear --verbose ./tests